# Random Techmeme Page

Get a random Techmeme page for you to explore tech news from back then. As long as Techmeme keeps the snapshot then it will be available to use viewed.

This site also generates a random time so you can see the news at different times of the day.

## Website
[projects.gregoryhammond.ca/random-techmeme-page/](https://projects.gregoryhammond.ca/random-techmeme-page/)

## License
[Unlicense](https://unlicense.org/)